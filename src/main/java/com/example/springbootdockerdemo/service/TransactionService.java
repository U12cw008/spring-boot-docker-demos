package com.example.springbootdockerdemo.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.springbootdockerdemo.entity.Transaction;
import com.example.springbootdockerdemo.repository.TransactionRepository;

@Service
public class TransactionService {

	@Autowired
	TransactionRepository transactionRepository;

	public void saveTransaction(Transaction transaction) {
		transactionRepository.save(transaction);
	}

	public Iterable<Transaction> getTransactionHistory() {
		return transactionRepository.findAll();
	}

	public Optional<Transaction> getTransaction(Long id) {
		return transactionRepository.findById(id);
	}

	public void deleteTransaction(Transaction transaction) {
		transactionRepository.delete(transaction);
	}

}