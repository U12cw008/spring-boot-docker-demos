package com.example.springbootdockerdemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.example.springbootdockerdemo.entity.Transaction;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long>{


}